package matchstick;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

public class TestTwoStations {

	@Rule
	public TestName name = new TestName();

	@Before
	public void setup() {
		System.out.println(name.getClass() + "::" + name.getMethodName());
	}

	@After
	public void teardown() {
		/* empty */}

	@Test
	public void testOneCycleFixed1() {

		Workline simpleSimulation = new Workline.Builder(20, true).build();
		simpleSimulation.addWorkStation("Dev");
		simpleSimulation.addWorkStation("Release");
		simpleSimulation.finalise();
		Random fixedDie = new FixedDie(1);
		simpleSimulation.getWorkStationList().get(0).setDie(fixedDie);
		simpleSimulation.getWorkStationList().get(1).setDie(fixedDie);
		simpleSimulation.runCycle();
		assertEquals(19, simpleSimulation.getBacklog().getOutputBufferSize());
		assertEquals(0, simpleSimulation.getWorkStationList().get(0).getOutputBufferSize());
		assertEquals(0, simpleSimulation.getWorkStationList().get(1).getOutputBufferSize());
		assertEquals(1, simpleSimulation.getProduction().getOutputBufferSize());
	}

	@Test
	public void testOneCycleFixed3() {
		Workline simpleSimulation = new Workline.Builder(20, true).build();
		simpleSimulation.addWorkStation("Dev");
		simpleSimulation.addWorkStation("Release");
		simpleSimulation.finalise();
		Random fixedDie = new FixedDie(3);
		simpleSimulation.getWorkStationList().get(0).setDie(fixedDie);
		simpleSimulation.getWorkStationList().get(1).setDie(fixedDie);
		simpleSimulation.runCycle();
		assertEquals(17, simpleSimulation.getBacklog().getOutputBufferSize());
		assertEquals(0, simpleSimulation.getWorkStationList().get(0).getOutputBufferSize());
		assertEquals(0, simpleSimulation.getWorkStationList().get(1).getOutputBufferSize());
		assertEquals(3, simpleSimulation.getProduction().getOutputBufferSize());
	}

	@Test
	public void testOneCycleFixed32() {
		Workline simpleSimulation = new Workline.Builder(20, true).build();
		simpleSimulation.addWorkStation("Dev");
		simpleSimulation.addWorkStation("Release");
		simpleSimulation.finalise();
		simpleSimulation.getWorkStationList().get(0).setDie(new FixedDie(3));
		simpleSimulation.getWorkStationList().get(1).setDie(new FixedDie(2));
		simpleSimulation.runCycle();
		assertEquals(17, simpleSimulation.getBacklog().getOutputBufferSize());
		assertEquals(1, simpleSimulation.getWorkStationList().get(0).getOutputBufferSize());
		assertEquals(0, simpleSimulation.getWorkStationList().get(1).getOutputBufferSize());
		assertEquals(2, simpleSimulation.getProduction().getOutputBufferSize());
	}
	
	@Test
	public void testEightCycleFixed32() {
		Workline simpleSimulation = new Workline.Builder(20, true).build();
		simpleSimulation.addWorkStation("Dev");
		simpleSimulation.addWorkStation("Release");
		simpleSimulation.finalise();
		simpleSimulation.getWorkStationList().get(0).setDie(new FixedDie(3));
		simpleSimulation.getWorkStationList().get(1).setDie(new FixedDie(2));
		
		simpleSimulation.runCycle();
		simpleSimulation.runCycle();
		simpleSimulation.runCycle();
		simpleSimulation.runCycle();
		simpleSimulation.runCycle();
		simpleSimulation.runCycle();
		simpleSimulation.runCycle();
		
		assertEquals(0, simpleSimulation.getBacklog().getOutputBufferSize());
		assertEquals(6, simpleSimulation.getWorkStationList().get(0).getOutputBufferSize());
		assertEquals(0, simpleSimulation.getWorkStationList().get(1).getOutputBufferSize());
		assertEquals(14, simpleSimulation.getProduction().getOutputBufferSize());
		

		simpleSimulation.runCycle();
		
		assertEquals(0, simpleSimulation.getBacklog().getOutputBufferSize());
		assertEquals(4, simpleSimulation.getWorkStationList().get(0).getOutputBufferSize());
		assertEquals(0, simpleSimulation.getWorkStationList().get(1).getOutputBufferSize());
		assertEquals(16, simpleSimulation.getProduction().getOutputBufferSize());
	}

	@Test
	public void testFullTwentyFixed1() {
		Workline simpleSimulation = new Workline.Builder(20, true).build();
		simpleSimulation.addWorkStation("Dev");
		simpleSimulation.addWorkStation("Release");
		simpleSimulation.finalise();
		Random fixedDie = new FixedDie(1);
		simpleSimulation.getWorkStationList().get(0).setDie(fixedDie);
		simpleSimulation.getWorkStationList().get(1).setDie(fixedDie);
		int cycleCount = (int)simpleSimulation.runToCompletion().getCycles();
		assertEquals(20, cycleCount);
	}

	@Test
	public void testFullTwentyFixed3() {
		Workline simpleSimulation = new Workline.Builder(20, true).build();
		simpleSimulation.addWorkStation("Dev");
		simpleSimulation.addWorkStation("Release");
		simpleSimulation.finalise();
		Random fixedDie = new FixedDie(3);
		simpleSimulation.getWorkStationList().get(0).setDie(fixedDie);
		simpleSimulation.getWorkStationList().get(1).setDie(fixedDie);
		int cycleCount = (int)simpleSimulation.runToCompletion().getCycles();
		assertEquals(7, cycleCount);
	}

	@Test
	public void testFullTwentyFixed32() {
		Workline simpleSimulation = new Workline.Builder(20, true).build();
		simpleSimulation.addWorkStation("Dev");
		simpleSimulation.addWorkStation("Release");
		simpleSimulation.finalise();
		simpleSimulation.getWorkStationList().get(0).setDie(new FixedDie(3));
		simpleSimulation.getWorkStationList().get(1).setDie(new FixedDie(2));
		int cycleCount = (int)simpleSimulation.runToCompletion().getCycles();
		assertEquals(10, cycleCount);
	}

}