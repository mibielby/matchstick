package matchstick;

import java.util.Random;

class FixedDie extends Random {

	private static final long serialVersionUID = 1378387514662381978L;
	private final int value;

	public FixedDie(int value) {
		this.value = value;
	}

	public int nextInt(int bound) {
		return value - 1;
	}
}
