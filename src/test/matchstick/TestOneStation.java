package matchstick;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

public class TestOneStation {

	@Rule
	public TestName name = new TestName();

	@Before
	public void setup() {
		System.out.println(name.getClass() + "::" + name.getMethodName());
	}

	@After
	public void teardown() {
		/* empty */}

	@Test
	public void testOneCycleFixed1() {
		Workline simpleSimulation = new Workline.Builder(20, true).build();
		simpleSimulation.addSingleWorkStation("Dev");
		Random fixedDie = new FixedDie(1);
		simpleSimulation.getWorkStationList().get(0).setDie(fixedDie);
		simpleSimulation.runCycle();
		assertEquals(19, simpleSimulation.getBacklog().getOutputBufferSize());
		assertEquals(0, simpleSimulation.getWorkStationList().get(0).getOutputBufferSize());
		assertEquals(1, simpleSimulation.getProduction().getOutputBufferSize());
	}
	
	@Test
	public void testOneCycleFixed3() {
		Workline simpleSimulation = new Workline.Builder(20, true).build();
		simpleSimulation.addSingleWorkStation("Dev");
		Random fixedDie = new FixedDie(3);
		simpleSimulation.getWorkStationList().get(0).setDie(fixedDie);
		simpleSimulation.runCycle();
		assertEquals(17, simpleSimulation.getBacklog().getOutputBufferSize());
		assertEquals(0, simpleSimulation.getWorkStationList().get(0).getOutputBufferSize());
		assertEquals(3, simpleSimulation.getProduction().getOutputBufferSize());
	}

	@Test
	public void testFullTwentyFixed1() {
		System.out.println("TestOneStation:testFullTwentyFixed1");
		Workline simpleSimulation = new Workline.Builder(20, true).build();
		simpleSimulation.addSingleWorkStation("Dev");
		Random fixedDie = new FixedDie(1);
		simpleSimulation.getWorkStationList().get(0).setDie(fixedDie);
		int cycleCount = (int)simpleSimulation.runToCompletion().getCycles();
		assertEquals(20, cycleCount);
	}
	
	@Test
	public void testFullTwentyFixed3() {
		System.out.println("TestOneStation:testFullTwentyFixed3");
		Workline simpleSimulation = new Workline.Builder(20, true).build();
		simpleSimulation.addSingleWorkStation("Dev");
		Random fixedDie = new FixedDie(3);
		simpleSimulation.getWorkStationList().get(0).setDie(fixedDie);
		int cycleCount = (int)simpleSimulation.runToCompletion().getCycles();
		assertEquals(7, cycleCount);
	}

}