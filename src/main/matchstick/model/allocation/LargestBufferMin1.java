package matchstick.model.allocation;

import java.util.List;

import matchstick.model.work.BaseWorkstation;

public class LargestBufferMin1 extends WorkerAllocation {

	@Override
	public void allocate(List<BaseWorkstation> workStationList) {
		int workerCount = 0; 
		if (workStationList.isEmpty()) {
			return;
		}
		
		BaseWorkstation maxBacklogWorkstation = workStationList.get(0);
		for (BaseWorkstation w : workStationList) {
			workerCount += w.getCount();
			if (maxBacklogWorkstation.getPrevious().getOutputBufferSize() < w.getPrevious().getOutputBufferSize()) {
				maxBacklogWorkstation = w;
			}
		}
		
		int startIdx = 0;
		for (int i = 0; i<workStationList.size(); i++) {
			if (workStationList.get(i).getPrevious().getOutputBufferSize() == 0) {
				startIdx++;
			} else {
				workerCount-= workStationList.size() - i;
				break;
			}
		}
		
		// Now we have the number of workers and the busiest station, allocate to it
		for (int i=0; i<startIdx; i++) {
			workStationList.get(i).setCount(0);
		}
		for (int i=startIdx; i<workStationList.size(); i++) {
			if (maxBacklogWorkstation == workStationList.get(i)) {
				workStationList.get(i).setCount(workerCount+1);
			} else {
				workStationList.get(i).setCount(1);
			}
		}
	}

	@Override
	public void printAllocation(int[] counts) {
		int workerCount = 0;
		for (int i : counts) {
			workerCount += i;
		}
		System.out.println(workerCount + " swarming (min1) workers for " + counts.length + " stations");
		
	}
}
