package matchstick.model.allocation;

import java.util.List;

import matchstick.model.work.BaseWorkstation;

public class RtLMin1 extends WorkerAllocation {

	@Override
	public void allocate(List<BaseWorkstation> workStationList) {
		int workerCount = 0;
		if (workStationList.isEmpty()) {
			return;
		}

		for (BaseWorkstation w : workStationList) {
			workerCount += w.getCount();
			w.setCount(1);
		}
		
		workerCount -= workStationList.size();
	
		// Now we have the number of workers and the busiest station, allocate to it
		for (int i=workStationList.size() -1; i>=0; i--) {
			if (workStationList.get(i).getPrevious().getOutputBufferSize() > 0) {
				workStationList.get(i).setCount(workerCount+1);
				break;
			}
		}
	}

	@Override
	public void printAllocation(int[] counts) {
		int workerCount = 0;
		for (int i : counts) {
			workerCount += i;
		}
		System.out.println(workerCount + " RtL (min1) workers for " + counts.length + " stations");

	}
}
