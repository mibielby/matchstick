package matchstick.model.allocation;

import java.util.List;

import matchstick.model.work.BaseWorkstation;

public abstract class WorkerAllocation {

	public abstract void allocate(List<BaseWorkstation> workStationList);
	
	public abstract void printAllocation(int[] counts);
	
	public void printAllocation(List<BaseWorkstation> workStationList) {
		System.out.print("Worker allocation: ");
		for (BaseWorkstation w : workStationList) {
			System.out.print(w.getCount() + " ");
		}
		System.out.println();
	}
}
