package matchstick.model.allocation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import matchstick.model.work.BaseWorkstation;

public class LargestBuffer extends WorkerAllocation {

	@Override
	public void allocate(List<BaseWorkstation> workStationList) {
		int workerCount = 0; 
		if (workStationList.isEmpty()) {
			return;
		}
		
		BaseWorkstation maxBacklogWorkstation = workStationList.get(0);
		for (BaseWorkstation w : workStationList) {
			workerCount += w.getCount();
			if (maxBacklogWorkstation.getPrevious().getOutputBufferSize() < w.getPrevious().getOutputBufferSize()) {
				maxBacklogWorkstation = w;
			}
		}
		
		// Now we have the number of workers and the busiest station, allocate to it
		
		for (BaseWorkstation w : workStationList) {
			if (maxBacklogWorkstation == w) {
				w.setCount(workerCount);
			} else {
				w.setCount(0);
			}
		}
	}

	@Override
	public void printAllocation(int[] counts) {
		int workerCount = 0;
		for (int i : counts) {
			workerCount += i;
		}
		System.out.println(workerCount + " swarming workers for " + counts.length + " stations");
		
	}
}
