package matchstick.model.allocation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import matchstick.model.work.Backlog;
import matchstick.model.work.BaseWorkstation;

public class PredictedWorkMin1 extends WorkerAllocation {

	@Override
	public void allocate(List<BaseWorkstation> workStationList) {
		int workerCount = 0; 
		if (workStationList.isEmpty()) {
			return;
		}
		
		int totalInFlight = 0;
		int[] remaining = new int[workStationList.size()];
		for (int i=0; i < workStationList.size(); i++) {
			BaseWorkstation w  = workStationList.get(i);
			workerCount += w.getCount();
//			if (Backlog.class.isInstance(w.getPrevious())) {
//				continue;
//			}
			remaining[i] = (w.getPrevious().getOutputBufferSize());
			totalInFlight += w.getPrevious().getOutputBufferSize();
			w.setCount(1);
		}
		
		int workReq = (int)(Math.floor((double)totalInFlight / (double)workerCount));
		
		for (int i=0; i < workStationList.size(); i++) {
//			if (Backlog.class.isInstance(w.getPrevious())) {
//				continue;
//			}
			remaining[i] -= workReq;
		}
		
		
		// Now we have the number of workers and the busiest station, allocate to it
		int workersLeft = workerCount -  workStationList.size();
		while (workersLeft > 0) {
			Integer maxRemaining = remaining[0];
			int maxRemainingIdx = 0;
			for (int i=0; i < workStationList.size(); i++) {
				if (maxRemaining < remaining[i]) {
					maxRemaining =  remaining[i];
					maxRemainingIdx = i;
				}
			}
			remaining[maxRemainingIdx] -= workReq;
			BaseWorkstation w  = workStationList.get(maxRemainingIdx);
			w.setCount(w.getCount()+1);
			workersLeft--;
		}
	}

	@Override
	public void printAllocation(int[] counts) {
		int workerCount = 0;
		for (int i : counts) {
			workerCount += i;
		}
		System.out.println(workerCount + " predicting (min1) workers for " + counts.length + " stations");
		
	}
}
