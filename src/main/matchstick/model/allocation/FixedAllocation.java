package matchstick.model.allocation;

import java.util.List;

import matchstick.model.work.BaseWorkstation;

public class FixedAllocation extends WorkerAllocation {

	@Override
	public void allocate(List<BaseWorkstation> workStationList) {
		return;
	}

	@Override
	public void printAllocation(int[] counts) {
		for (int i : counts) {
			System.out.print(i + " ");
			if (i<=0) {
				System.err.println("Invalid worker count: " + i);
				System.exit(1);
			}
		}
		System.out.println();
		
	}
	
	@Override
	public void printAllocation(List<BaseWorkstation> workStationList) { /* EMPTY */ }

}
