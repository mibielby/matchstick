package matchstick.model.work;

public class Backlog extends BaseWorkstation {

	public Backlog(int outputBufferSize) {
		super(null, null, "Backlog", outputBufferSize);
	}

	@Override
	public int doWork() {
		return 0;
	}

	@Override
	public void increaseOutputBuffer(int workDone) { /* EMPTY */ }
	
	@Override
	public int getCount() {
		return 0;
	}

	@Override
	public void setCount(int count) { /* EMPTY */ }

	@Override
	public void addToCost(int count) { /* EMPTY */ }

	@Override
	public int getCost() {
		return 0;
	}
}
