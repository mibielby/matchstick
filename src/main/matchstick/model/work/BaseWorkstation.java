package matchstick.model.work;

import java.util.Random;

public abstract class BaseWorkstation {
	
	private BaseWorkstation previous;
	private BaseWorkstation next;
	private final String name;
	private int outputBufferSize;
	
	
	public BaseWorkstation(BaseWorkstation previous, BaseWorkstation next, String name, int outputBufferSize) {
		this.previous = previous;
		this.next = next;
		this.outputBufferSize = outputBufferSize;
		this.name = name;
	}

	public void setPrevious(BaseWorkstation w) { /*EMPTY*/ }
	protected void setPreviousInternal(BaseWorkstation w) { previous = w; }

	public void setNext(BaseWorkstation w) { /*EMPTY*/ }
	public void setNextInternal(BaseWorkstation w) { next = w; }
	
	public BaseWorkstation getPrevious() {
		return previous;
	}


	public BaseWorkstation getNext() {
		return next;
	}

	public int getOutputBufferSize() {
		return outputBufferSize;
	}
	
	public String getName() {
		return name;
	}
	
	public void setDie(Random die) { /* EMPTY */ }


	public abstract int doWork();
	public void doWork(int workDone) {/*EMPTY*/}
	
	public void reduceOutputBuffer(int workDone) {
		outputBufferSize -= workDone; //TODO bounds checking
	}
	
	public void increaseOutputBuffer(int workDone) {
		outputBufferSize += workDone;
	}
	
	public abstract int getCount();
	public abstract void setCount(int count);
	
	public abstract void addToCost(int add);
	public abstract int getCost();
	
}

