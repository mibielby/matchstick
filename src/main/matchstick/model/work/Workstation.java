package matchstick.model.work;

import java.util.Random;

public class Workstation extends BaseWorkstation {

	private Random die;
	private int dieLim = 6;
	private int count;

	private int cost; // measure of total output laid out at a station

	public Workstation(BaseWorkstation previous, BaseWorkstation next, String name, int outputBufferSize, int count) {
		super(previous, next, name, outputBufferSize);
		die = new Random();
		this.count = count;
		cost = 0;
	}

	@Override
	public void setDie(Random die) {
		this.die = die;
	}

	public void setNext(BaseWorkstation w) {
		setNextInternal(w);
	}

	@Override
	public int doWork() {
		int totalWorkDone = 0;
		for (int i=0; i<count; i++) {
			int workDone = die.nextInt(dieLim) + 1; // 0 <= int < dieLim
			workDone = Math.min(workDone, getPrevious().getOutputBufferSize());
			getPrevious().reduceOutputBuffer(workDone);
			increaseOutputBuffer(workDone);
			totalWorkDone += workDone;
		}
		return totalWorkDone;
	}

	@Override
	public int getCount() {
		return count;
	}

	@Override
	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public void addToCost(int add) {
		cost += add;
	}

	@Override
	public int getCost() {
		return cost;
	}

}
