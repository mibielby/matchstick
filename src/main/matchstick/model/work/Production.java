package matchstick.model.work;

public class Production extends BaseWorkstation {

	public Production() {
		super(null, null, "Production", 0);
	}
	
	public void setPrevious(BaseWorkstation w) {
		setPreviousInternal(w);
	}
	
	@Override
	public int doWork() {
		return 0;
	}
	
	@Override
	public void doWork(int workDone) {
		increaseOutputBuffer(workDone);
		getPrevious().reduceOutputBuffer(workDone);
	}
	
	@Override
	public void reduceOutputBuffer(int workDone) { /* EMPTY */ }

	@Override
	public int getCount() {
		return 0;
	}

	@Override
	public void setCount(int count) { /* EMPTY */ }

	@Override
	public void addToCost(int count) { /* EMPTY */ }

	@Override
	public int getCost() {
		return 0;
	}



}
