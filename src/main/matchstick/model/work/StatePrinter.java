package matchstick.model.work;

import java.util.List;

public class StatePrinter {

	private enum PrintTypes {
		WAS_NOW, // what the output buffer was before and after the stage worked
		PRODUCED_REMAINING  // what the output buffer was after the stage worked and after the following stage worked
	}

	private PrintTypes printType;

	public StatePrinter(PrintTypes printType) {
		this.printType = printType;
	}

	public StatePrinter() {
		this.printType = PrintTypes.PRODUCED_REMAINING; //default to this
	}

	public void printState(List<Integer> beforeList, List<Integer> afterList, List<Integer> workDoneList, List<Integer> costList) {
		switch (printType) {
		case WAS_NOW:
			printStateWasNow(beforeList, afterList);
			break;
		case PRODUCED_REMAINING:
			printStateProducedRemaining(beforeList, afterList, workDoneList, costList);
			break;
		default:
			System.err.println("Unknown print type");
			System.exit(1);
		}
	}

	private void printStateWasNow(List<Integer> beforeList, List<Integer> afterList) {
		int i = 0;
		int workDone = 0;
		System.out.print(beforeList.get(i) + " / " + afterList.get(i) + " |");
		i++;
		while (i <= beforeList.size() - 3) {
			workDone = afterList.get(i) - beforeList.get(i);
			System.out.print("| " + workDone + " | " + beforeList.get(i) + " / " + afterList.get(i) + " ");
			i++;
		}
		workDone = afterList.get(i) - beforeList.get(i);
		System.out.print("| " + workDone);
		i++;
		System.out.println(" || " + beforeList.get(i) + " / " + afterList.get(i));
	}
	
	private void printStateProducedRemaining(List<Integer> beforeList, List<Integer> afterList, List<Integer> workDoneList,
			List<Integer> costList) {
		int i = 0;
		int workDone = 0;
		System.out.print("| " + printFixedWidth(beforeList.get(i)) + " |");
		i++;
		while (i <= beforeList.size() - 3) {
			workDone = afterList.get(i) - beforeList.get(i);
			System.out.print("| " + printFixedWidth(workDone) + " ");
			System.out.print(" [" + printFixedWidth(costList.get(i)) + "] ");
			
			String remainingOutputBuffer = printFixedWidth(afterList.get(i)-workDoneList.get(i));
			System.out.print("| " + printFixedWidth(afterList.get(i)) + " / " + remainingOutputBuffer + " "); 
			i++;
		}
		workDone = afterList.get(i) - beforeList.get(i);
		System.out.print("| " + printFixedWidth(afterList.get(i)));
		i++;
		System.out.println(" | " + printFixedWidth(afterList.get(i)) +" |");
	}
	
	private String printFixedWidth(int i) {
		return String.format("%2s", i);
	}
}
