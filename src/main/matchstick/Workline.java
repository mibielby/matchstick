package matchstick;

import java.util.ArrayList;
import java.util.List;

import matchstick.model.allocation.FixedAllocation;
import matchstick.model.allocation.WorkerAllocation;
import matchstick.model.work.Backlog;
import matchstick.model.work.BaseWorkstation;
import matchstick.model.work.Production;
import matchstick.model.work.StatePrinter;
import matchstick.model.work.Workstation;

public class Workline {

	public enum RunMode {
		BACKLOG_BOUD, CYCLE_BOUND
	}

	private final Backlog backlog;
	private final Production production;
	private final StatePrinter statePrinter = new StatePrinter();
	private final RunMode mode;

	private final int backlogSize; // Used to set the available "work" to process
	private int cycleLimit; // Used to set the available cycles to process for
	private List<BaseWorkstation> workStationList;
	private final WorkerAllocation workerAllocation;

	private boolean finalised = false;
	private boolean debug = false;

	private Workline(int backlogSize, int cycleLimit, WorkerAllocation workerAllocation, boolean debug, RunMode mode) {
		this.backlogSize = backlogSize;
		this.cycleLimit = cycleLimit;
		workStationList = new ArrayList<>();
		backlog = new Backlog(this.backlogSize);
		production = new Production();
		this.workerAllocation = workerAllocation;
		this.debug = debug;
		this.mode = mode;
	}

	public Backlog getBacklog() {
		return backlog;
	}

	public Production getProduction() {
		return production;
	}

	public List<BaseWorkstation> getWorkStationList() {
		return workStationList;
	}

	public void addSingleWorkStation(String name) {
		workStationList.add(new Workstation(backlog, production, name, 0, 1));
		finalise();
	}

	public void addWorkStation(String name, int count) {

		BaseWorkstation workstation = null;

		if (workStationList.isEmpty()) {
			workstation = new Workstation(backlog, null, name, 0, count);
		} else {
			BaseWorkstation prevStation = workStationList.get(workStationList.size() - 1);
			workstation = new Workstation(prevStation, null, name, 0, count);
			prevStation.setNext(workstation);
		}

		workStationList.add(workstation);
	}

	public void addWorkStation(String name) {
		addWorkStation(name, 1);
	}

	public void finalise() {
		if (finalised) {
			System.err.println("Already finalized");
			System.exit(1);
		}
		BaseWorkstation prevWorkstation = workStationList.get(workStationList.size() - 1);
		production.setPrevious(prevWorkstation);
		prevWorkstation.setNext(production);
		finalised = true;
	}

	public SimulationResult runToCompletion() {
		if (!finalised) {
			System.err.println("Not finalized");
			System.exit(1);
		}

		int cycles = 0;
		int totalCost = 0;
		
		switch(mode) {
			case BACKLOG_BOUD:
				while (production.getOutputBufferSize() != backlogSize) {
					runCycle();
					cycles++;
				}
				break;
			case CYCLE_BOUND:
				while (cycles < cycleLimit) {
					runCycle();
					cycles++;
					cycleLimit--;
				}
		}
		
		for (BaseWorkstation w : workStationList) {
			totalCost += w.getCost();
		}

		return new SimulationResult(cycles, totalCost);
	}

	public void runCycle() {
		if (!finalised) {
			System.err.println("Not finalized");
			System.exit(1);
		}

		workerAllocation.allocate(workStationList);
		if (debug) {
			workerAllocation.printAllocation(workStationList);
		}

		ArrayList<Integer> beforeList = new ArrayList<>();
		ArrayList<Integer> afterList = new ArrayList<>();
		ArrayList<Integer> workDoneList = new ArrayList<>();
		ArrayList<Integer> costList = new ArrayList<>();

		int workDone = 0;
		beforeList.add(backlog.getOutputBufferSize());

		for (BaseWorkstation w : workStationList) {

			// Work to do at this station
			beforeList.add(w.getOutputBufferSize());

			if (w.getPrevious().getOutputBufferSize() > 0) {
				workDone = w.doWork();
			} else {
				workDone = 0;
			}

			workDoneList.add(workDone);
			afterList.add(w.getOutputBufferSize());
			if (w.getPrevious() != backlog) {
				w.getPrevious().addToCost(w.getPrevious().getOutputBufferSize());
			}
			costList.add(w.getPrevious().getCost());
		}

		afterList.add(0, backlog.getOutputBufferSize());
		beforeList.add(production.getOutputBufferSize());
		production.doWork(workDone);
		afterList.add(production.getOutputBufferSize());

		if (debug) {
			statePrinter.printState(beforeList, afterList, workDoneList, costList);
		}
	}

	public static class Builder {
		WorkerAllocation workerAllocation = new FixedAllocation();
		int backlogSize;
		int cycleLimit;
		boolean debug;
		RunMode mode;

		Builder(int size, boolean debug, RunMode mode) {
			this.mode = mode;
			switch (this.mode) {
			case BACKLOG_BOUD:
				this.backlogSize = size;
				this.cycleLimit = 0;
				break;
			case CYCLE_BOUND:
				this.backlogSize = Integer.MAX_VALUE; // Silly high number as its not used
				this.cycleLimit = size;
				break;
			}
			
			this.debug = debug;
		}

		public Builder setWorkerAllocation(WorkerAllocation workerAllocation) {
			this.workerAllocation = workerAllocation;
			return this;
		}

		public Workline build() {
			return new Workline(backlogSize, cycleLimit, workerAllocation, debug, mode);
		}
	}

}
