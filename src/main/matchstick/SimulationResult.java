package matchstick;

public class SimulationResult {

	private final double cycles;
	private final double cost;
	
	public SimulationResult(double cycles, double cost) {
		super();
		this.cycles = cycles;
		this.cost = cost;
	}

	public double getCycles() {
		return cycles;
	}

	public double getCost() {
		return cost;
	}
	
	
	
}
