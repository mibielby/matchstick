package matchstick;

import matchstick.model.allocation.FixedAllocation;
import matchstick.model.allocation.WorkerAllocation;

public class Simulation {

	private final int limit; // number of times to run the simulation
	private final int[] sizes; // backlog sizes
	private final int[] counts; // count of workers at each station
	private final String[] names; // names for each station
	private WorkerAllocation workerAllocation; // how to reallocate the workers each cycle

	public Simulation(int lim, int[] sizes, int[] counts, String[] names) {
		if (counts.length != names.length) {
			System.err.println("Count length didn't match name length");
			System.exit(1);
		}
		this.limit = lim;
		this.sizes = sizes.clone();
		this.counts = counts.clone();
		this.names = names.clone();
		this.workerAllocation = new FixedAllocation();
	}

	public Simulation(int lim, int[] sizes, int[] counts, String[] names, WorkerAllocation workerAllocation) {
		if (counts.length != names.length) {
			System.err.println("Count length didn't match name length");
			System.exit(1);
		}
		this.limit = lim;
		this.sizes = sizes.clone();
		this.counts = counts.clone();
		this.names = names.clone();
		this.workerAllocation = workerAllocation;
	}

	public void run(boolean debug) {
		System.out.print("Running simulations: ");
		workerAllocation.printAllocation(counts);

		for (int i : sizes) {
			SimulationResult result = runOneSize(i, debug);
			System.out.println("££ Avg cost for backlog size " + i + " was " + String.format("%.2f", result.getCost()));
			System.out.println("@@ Avg cycles for backlog size " + i + " was " + String.format("%.2f", result.getCycles()) + " cycles");
		}

		System.out.println("----");
	}

	private SimulationResult runOneSize(int size, boolean debug) {
		double avgCycles = 0;
		double avgCost = 0;
		for (int i = 0; i < limit; i++) {
			SimulationResult result = runOnce(size, debug);
			avgCycles = (avgCycles * (i) + result.getCycles()) / (i + 1);
			avgCost = (avgCost * (i) + result.getCost()) / (i + 1);
		}
		return new SimulationResult(avgCycles, avgCost);
	}

	private SimulationResult runOnce(int size, boolean debug) {
		Workline simpleSimulation = new Workline.Builder(size, debug).setWorkerAllocation(workerAllocation).build();
		for (int i = 0; i < counts.length; i++) {
			simpleSimulation.addWorkStation(names[i], counts[i]);
		}
		simpleSimulation.finalise();
		SimulationResult result = simpleSimulation.runToCompletion();
//		System.out.println("## Took " + cycleCount + " cycles");
//		System.out.println("££ Total cost was " + totalCost);
		return result;
	}
}
