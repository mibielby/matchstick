package matchstick;

import java.util.ArrayList;

import matchstick.Workline.RunMode;
import matchstick.model.allocation.LargestBuffer;
import matchstick.model.allocation.LargestBufferExBacklog;
import matchstick.model.allocation.LargestBufferMin1;
import matchstick.model.allocation.PredictedWork;
import matchstick.model.allocation.PredictedWorkMin1;
import matchstick.model.allocation.PredictedWorkRtL;
import matchstick.model.allocation.RtL;
import matchstick.model.allocation.RtLMin1;
import matchstick.model.allocation.WorkerAllocation;

public class Matchsticks {

	private static final int LIMIT = 1000000;

	private static int[] sizes = { 50 };
	private static final boolean DEBUG = false;
	private static final RunMode MODE = RunMode.CYCLE_BOUND;

	public static void main(String[] args) throws Exception {

//		Simulation sim = new Simulation(LIMIT, sizes, new int[]{2,2,2}, new String[]{"Dev", "QA", "Prod"}, new PredictedWorkRtL());
//		sim.run(DEBUG);

		make3s();

	}

	private static void make3s() {
		threesWorker(3);
		threesWorker(4);
		threesWorker(5);
		threesWorker(6);
	}

	private static void threesWorker(int count) {
		String[] names = new String[] { "Dev", "QA", "Prod" };
		ArrayList<int[]> countList = make3List(count);
		for (int[] a : countList) {
			Simulation sim = new Simulation(LIMIT, sizes, a, names);
			sim.run(DEBUG);
		}
		runOtherTypes(sizes, countList.get(0), names);
		System.out.println("--------------------");
	}

	private static void runOtherTypes(int[] sizes, int[] counts, String[] names) {
		WorkerAllocation[] allocations = { new LargestBuffer(), new LargestBufferExBacklog(), new LargestBufferMin1(),
				new RtL(), new RtLMin1(), new PredictedWork(), new PredictedWorkMin1(), new PredictedWorkRtL()};
		for (WorkerAllocation w : allocations) {
			Simulation sim = new Simulation(LIMIT, sizes, counts, names, w);
			sim.run(DEBUG);
		}
	}

	private static ArrayList<int[]> make3List(int count) {
		ArrayList<int[]> list = new ArrayList<>();
		for (int start = count - 2; start > 0; start--) {
			for (int mid = Math.max(count - start - 1, 1); mid > 0; mid--) {
				list.add(new int[] { start, mid, count - start - mid });
			}
		}

		return list;

	}

}
