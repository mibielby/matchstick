Averaged over 1,000,000 simulations, each running a backlog of 50 items (each worker doing 1-6 work)

Running simulations: 1 1 1 
££ Avg cost for backlog size 50 was 98.76
@@ Avg cycles for backlog size 50 was 17.87 cycles
----
Running simulations: 3 swarming workers for 3 stations
££ Avg cost for backlog size 50 was 316.38
@@ Avg cycles for backlog size 50 was 16.41 cycles
----
Running simulations: 3 (ex-Backlog) swarming workers for 3 stations
££ Avg cost for backlog size 50 was 119.66
@@ Avg cycles for backlog size 50 was 20.83 cycles
----
Running simulations: 3 swarming (min1) workers for 3 stations
££ Avg cost for backlog size 50 was 92.43
@@ Avg cycles for backlog size 50 was 16.73 cycles
----
Running simulations: 3 RtL workers for 3 stations
££ Avg cost for backlog size 50 was 120.02
@@ Avg cycles for backlog size 50 was 21.27 cycles
----
Running simulations: 3 RtL (min1) workers for 3 stations
££ Avg cost for backlog size 50 was 98.72
@@ Avg cycles for backlog size 50 was 17.87 cycles
----
Running simulations: 3 predicting workers for 3 stations
££ Avg cost for backlog size 50 was 322.07
@@ Avg cycles for backlog size 50 was 15.88 cycles
----
Running simulations: 3 predicting (min1) workers for 3 stations
££ Avg cost for backlog size 50 was 98.77
@@ Avg cycles for backlog size 50 was 17.87 cycles
----
Running simulations: 3 predicting (RtL) workers for 3 stations
££ Avg cost for backlog size 50 was 175.86
@@ Avg cycles for backlog size 50 was 16.78 cycles
----
--------------------
Running simulations: 2 1 1 
££ Avg cost for backlog size 50 was 240.94
@@ Avg cycles for backlog size 50 was 16.63 cycles
----
Running simulations: 1 2 1 
££ Avg cost for backlog size 50 was 59.34
@@ Avg cycles for backlog size 50 was 16.62 cycles
----
Running simulations: 1 1 2 
££ Avg cost for backlog size 50 was 59.65
@@ Avg cycles for backlog size 50 was 16.63 cycles
----
Running simulations: 4 swarming workers for 3 stations
££ Avg cost for backlog size 50 was 252.41
@@ Avg cycles for backlog size 50 was 12.85 cycles
----
Running simulations: 4 (ex-Backlog) swarming workers for 3 stations
££ Avg cost for backlog size 50 was 116.16
@@ Avg cycles for backlog size 50 was 15.92 cycles
----
Running simulations: 4 swarming (min1) workers for 3 stations
££ Avg cost for backlog size 50 was 163.58
@@ Avg cycles for backlog size 50 was 11.83 cycles
----
Running simulations: 4 RtL workers for 3 stations
££ Avg cost for backlog size 50 was 116.47
@@ Avg cycles for backlog size 50 was 16.19 cycles
----
Running simulations: 4 RtL (min1) workers for 3 stations
££ Avg cost for backlog size 50 was 49.92
@@ Avg cycles for backlog size 50 was 13.16 cycles
----
Running simulations: 4 predicting workers for 3 stations
££ Avg cost for backlog size 50 was 250.45
@@ Avg cycles for backlog size 50 was 12.43 cycles
----
Running simulations: 4 predicting (min1) workers for 3 stations
££ Avg cost for backlog size 50 was 172.35
@@ Avg cycles for backlog size 50 was 12.68 cycles
----
Running simulations: 4 predicting (RtL) workers for 3 stations
££ Avg cost for backlog size 50 was 163.41
@@ Avg cycles for backlog size 50 was 13.13 cycles
----
--------------------
Running simulations: 3 1 1 
££ Avg cost for backlog size 50 was 299.10
@@ Avg cycles for backlog size 50 was 16.58 cycles
----
Running simulations: 2 2 1 
££ Avg cost for backlog size 50 was 186.93
@@ Avg cycles for backlog size 50 was 14.89 cycles
----
Running simulations: 2 1 2 
££ Avg cost for backlog size 50 was 186.78
@@ Avg cycles for backlog size 50 was 14.88 cycles
----
Running simulations: 1 3 1 
££ Avg cost for backlog size 50 was 57.57
@@ Avg cycles for backlog size 50 was 16.58 cycles
----
Running simulations: 1 2 2 
££ Avg cost for backlog size 50 was 5.70
@@ Avg cycles for backlog size 50 was 14.89 cycles
----
Running simulations: 1 1 3 
££ Avg cost for backlog size 50 was 57.64
@@ Avg cycles for backlog size 50 was 16.58 cycles
----
Running simulations: 5 swarming workers for 3 stations
££ Avg cost for backlog size 50 was 210.44
@@ Avg cycles for backlog size 50 was 10.62 cycles
----
Running simulations: 5 (ex-Backlog) swarming workers for 3 stations
££ Avg cost for backlog size 50 was 113.61
@@ Avg cycles for backlog size 50 was 12.85 cycles
----
Running simulations: 5 swarming (min1) workers for 3 stations
££ Avg cost for backlog size 50 was 159.77
@@ Avg cycles for backlog size 50 was 9.63 cycles
----
Running simulations: 5 RtL workers for 3 stations
££ Avg cost for backlog size 50 was 113.91
@@ Avg cycles for backlog size 50 was 13.04 cycles
----
Running simulations: 5 RtL (min1) workers for 3 stations
££ Avg cost for backlog size 50 was 53.88
@@ Avg cycles for backlog size 50 was 11.28 cycles
----
Running simulations: 5 predicting workers for 3 stations
££ Avg cost for backlog size 50 was 205.20
@@ Avg cycles for backlog size 50 was 10.38 cycles
----
Running simulations: 5 predicting (min1) workers for 3 stations
££ Avg cost for backlog size 50 was 164.99
@@ Avg cycles for backlog size 50 was 9.98 cycles
----
Running simulations: 5 predicting (RtL) workers for 3 stations
££ Avg cost for backlog size 50 was 153.49
@@ Avg cycles for backlog size 50 was 11.03 cycles
----
--------------------
Running simulations: 4 1 1 
££ Avg cost for backlog size 50 was 329.07
@@ Avg cycles for backlog size 50 was 16.58 cycles
----
Running simulations: 3 2 1 
££ Avg cost for backlog size 50 was 244.43
@@ Avg cycles for backlog size 50 was 14.82 cycles
----
Running simulations: 3 1 2 
££ Avg cost for backlog size 50 was 244.50
@@ Avg cycles for backlog size 50 was 14.83 cycles
----
Running simulations: 2 3 1 
££ Avg cost for backlog size 50 was 184.28
@@ Avg cycles for backlog size 50 was 14.83 cycles
----
Running simulations: 2 2 2 
££ Avg cost for backlog size 50 was 42.74
@@ Avg cycles for backlog size 50 was 9.04 cycles
----
Running simulations: 2 1 3 
££ Avg cost for backlog size 50 was 184.36
@@ Avg cycles for backlog size 50 was 14.83 cycles
----
Running simulations: 1 4 1 
££ Avg cost for backlog size 50 was 57.48
@@ Avg cycles for backlog size 50 was 16.58 cycles
----
Running simulations: 1 3 2 
££ Avg cost for backlog size 50 was 3.06
@@ Avg cycles for backlog size 50 was 14.83 cycles
----
Running simulations: 1 2 3 
££ Avg cost for backlog size 50 was 3.09
@@ Avg cycles for backlog size 50 was 14.83 cycles
----
Running simulations: 1 1 4 
££ Avg cost for backlog size 50 was 57.48
@@ Avg cycles for backlog size 50 was 16.58 cycles
----
Running simulations: 6 swarming workers for 3 stations
££ Avg cost for backlog size 50 was 186.33
@@ Avg cycles for backlog size 50 was 9.40 cycles
----
Running simulations: 6 (ex-Backlog) swarming workers for 3 stations
££ Avg cost for backlog size 50 was 112.31
@@ Avg cycles for backlog size 50 was 11.14 cycles
----
Running simulations: 6 swarming (min1) workers for 3 stations
££ Avg cost for backlog size 50 was 147.40
@@ Avg cycles for backlog size 50 was 8.23 cycles
----
Running simulations: 6 RtL workers for 3 stations
££ Avg cost for backlog size 50 was 112.64
@@ Avg cycles for backlog size 50 was 11.30 cycles
----
Running simulations: 6 RtL (min1) workers for 3 stations
££ Avg cost for backlog size 50 was 61.53
@@ Avg cycles for backlog size 50 was 9.99 cycles
----
Running simulations: 6 predicting workers for 3 stations
££ Avg cost for backlog size 50 was 177.56
@@ Avg cycles for backlog size 50 was 9.05 cycles
----
Running simulations: 6 predicting (min1) workers for 3 stations
££ Avg cost for backlog size 50 was 149.83
@@ Avg cycles for backlog size 50 was 8.33 cycles
----
Running simulations: 6 predicting (RtL) workers for 3 stations
££ Avg cost for backlog size 50 was 139.60
@@ Avg cycles for backlog size 50 was 9.68 cycles
----
--------------------



#########################################

Averaged over 1,000,000 simulations, each running a backlog of 100 items (each worker doing 1-6 work)

Running simulations: 1 1 1 
@@ Avg for backlog size 100 was 33.61 cycles
----
--------------------
Running simulations: 2 1 1 
@@ Avg for backlog size 100 was 31.78 cycles
----
Running simulations: 1 2 1 
@@ Avg for backlog size 100 was 31.77 cycles
----
Running simulations: 1 1 2 
@@ Avg for backlog size 100 was 31.78 cycles
----
--------------------
Running simulations: 3 1 1 
@@ Avg for backlog size 100 was 31.73 cycles
----
Running simulations: 1 3 1 
@@ Avg for backlog size 100 was 31.73 cycles
----
Running simulations: 1 1 3 
@@ Avg for backlog size 100 was 31.73 cycles
----
Running simulations: 1 2 2 
@@ Avg for backlog size 100 was 29.17 cycles
----
Running simulations: 2 1 2 
@@ Avg for backlog size 100 was 29.17 cycles
----
Running simulations: 2 2 1 
@@ Avg for backlog size 100 was 29.17 cycles
----
--------------------
Running simulations: 4 1 1 
@@ Avg for backlog size 100 was 31.73 cycles
----
Running simulations: 1 4 1 
@@ Avg for backlog size 100 was 31.72 cycles
----
Running simulations: 1 1 4 
@@ Avg for backlog size 100 was 31.72 cycles
----
Running simulations: 3 2 1 
@@ Avg for backlog size 100 was 29.12 cycles
----
Running simulations: 3 1 2 
@@ Avg for backlog size 100 was 29.12 cycles
----
Running simulations: 2 3 1 
@@ Avg for backlog size 100 was 29.11 cycles
----
Running simulations: 1 3 2 
@@ Avg for backlog size 100 was 29.11 cycles
----
Running simulations: 1 2 3 
@@ Avg for backlog size 100 was 29.12 cycles
----
Running simulations: 2 1 3 
@@ Avg for backlog size 100 was 29.11 cycles
----
Running simulations: 2 2 2 
@@ Avg for backlog size 100 was 16.90 cycles
----

#########################################
